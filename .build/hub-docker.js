const fs = require('fs');
const simpleGit = require('simple-git');
const YAML = require('yaml');
const baseUrl = 'https://hub.docker.com';
const baseGitlabUrl = 'https://gitlab.com/api/v4';
const tagPatterns = ['dev', 'rc', 'latest', 'beta', 'stable', 'b', '-'];

function copyFiles(src, dst) {
    return new Promise((resolve, reject) => {
        fs.copyFile(src, dst, (err) => {
            if (err) {
                reject(err);
                console.error(err);
                return err;
            }
            resolve();
        })
    });
}

function tagFilter(item) {
    const res = !tagPatterns.find(f => item.name.toLowerCase().indexOf(f) >= 0);
    if (!res) {
        return false;
    }
    return item.name.split('.').length === 3;
}

function getApiURL(path) {
    return `${baseUrl}${path}`;
}

function getGitlabApiURL(path) {
    return `${baseGitlabUrl}${path}`;
}

function fetchData(url, token, body, method = 'GET') {
    const params = {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow'
    };
    if (token) {
        params.headers['Authorization'] = `JWT ${token}`;
    }
    if (body) {
        params.body = JSON.stringify(body);
    }
    return fetch(getApiURL(url), params)
        .then(response =>  {
            //response.json().then(data => console.log(data));
            console.log(response.url);
            console.log(response.statusText);
            return response.json();
        })
        .catch(err => {
            console.error({err: err, url: url})
            throw err;
        });
}

function fetchGitlab(url, token) {
    const params = {
        method: 'Get',
        headers: {
            'Content-Type': 'application/json',
            'PRIVATE-TOKEN': token
        },
        redirect: 'follow'
    };
    return fetch(getGitlabApiURL(url), params)
        .then(response => response.json())
        .catch(err => {
            console.error({err: err, url: url})
            throw err;
        });
}

function login(username, password) {
    return fetchData('/v2/users/login/', undefined, {username, password}, 'POST')
        .then(data => data.token);
}

function getImageTags(image, token, filters) {
    if (image.indexOf('/') < 0) {
        image = 'library/' + image;
    }
    return fetchData(`/v2/repositories/${image}/tags/?page_size=100`, token)
        .then(
            data => data.results
                .map(item => ({id: item.id, name: item.name, status: item.tag_status}))
                .filter(item => item.status === 'active')
                .filter(filters)
        );
}

function readFile(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf8', (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data.toString());
        })
    });
}

function writeFile(file, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, 'utf8', (err) => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        })
    });
}

function bumpVersion(folder) {
    const file = `${folder}/Chart.yaml`;
    return readFile(file)
        .then(data => YAML.parse(data))
        .then(data => {
            const v = data.version.split('.');
            v[1] = (parseInt(v[1], 10) + 1).toString();
            v[2] = '0'
            data.version = v.join('.');
            return data;
        })
        .then(data => writeFile(file, YAML.stringify(data)));
}

function updateYamlValues(folder, token) {
    let isUpdated = false;
    const file = `${folder}/values.yaml`;
    return readFile(file)
        .then(txt => YAML.parse(txt))
        .then(async values =>  {
            const list = Object.keys(values.services).map(key => values.services[key]);
            await Promise.all(
                list.map(async item => {
                    const tags = await getImageTags(item.image, token, tagFilter);
                    if (item.version !== tags[0].name) {
                        item.version = tags[0].name;
                        isUpdated = true;
                    }
                })
            );
            return values;
        })
        .then(async values => {
            if (isUpdated) {
                await writeFile(file, YAML.stringify(values));
                await bumpVersion(folder);
                return true;
            }
            return false;
        });
}

async function git(folders) {
    const git = simpleGit();
    await git.addConfig('user.name', 'Aleksey Grinevich').addConfig('user.email', 'aleksay@gmail.com');
    console.log('clone');
    await git.clone('git@gitlab.com:shadowy/repository.git', '.rep');
    console.log('copy');
    await Promise.all(folders.map(folder =>
        copyFiles(`${folder}//values.yaml`, `.rep/${folder}//values.yaml`)
            .then(() => copyFiles(`${folder}//Chart.yaml`, `.rep/${folder}//Chart.yaml`)))
    );
    console.log('add');
    await git.add('.');
    console.log('commit');
    await git.commit('upgrade docker images version');
    console.log('push');
    await git.push('origin', 'main');
}

async function checkGitlab(image, version, token) {
    const list = await fetchGitlab(`/projects/31935422/registry/repositories`, token);
    if (list.length === 0) {
        return false;
    }
    const containerId = list.find(item => item.name === image).id;
    const tags = await fetchGitlab(`/projects/31935422/registry/repositories/${containerId}/tags`, token);
    return tags.findIndex(item => item.name === version) >= 0;
}

exports.tagFilter = tagFilter;
exports.getImageTags = getImageTags;
exports.login = login;
exports.updateYamlValues = updateYamlValues;
exports.git = git;
exports.checkGitlab = checkGitlab;
