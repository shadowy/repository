const docker = require('./hub-docker');
const { exec } = require('child_process');

async function main() {
    const image = process.argv[2];
    const token = await docker.login(process.env.DOCKER_HUB_USER, process.env.DOCKER_HUB_PASSWORD);
    const res = await docker.getImageTags( image, token, docker.tagFilter);
    const part = image.split('/');
    const name = part.length === 2 ? part[1] : part[0];
    const isExist = await docker.checkGitlab(name, res[0].name, process.env.GITLAB_TOKEN);
    console.log(`is exist: ${isExist}`);
    if (!isExist) {
        exec(`./docker/build.sh ${name} ${res[0].name}`, (error) => {
            if (error) {
                console.error(error)
                process.exit(1);
            }
        });
    }
}

void main();
