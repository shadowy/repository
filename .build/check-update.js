const simpleGit = require('simple-git');
const docker = require('./hub-docker');
simpleGit().clean(simpleGit.CleanOptions.FORCE);


async function update(paths) {
    console.log(`Update: ${paths}`);
    const token = await docker.login(process.env.DOCKER_USER_NAME, process.env.DOCKER_PASSWORD);
    const res = await Promise.all(paths.map(path => docker.updateYamlValues(path, token)));
    await docker.git(paths);
    //}
    console.log(res);
}

void update(['./home-assistant', './nginx']);
