#!/bin/sh
#docker build --build-arg version=$2 -t registry.gitlab.com/shadowy/repository/$1:$2 -f ./docker/$1.dockerfile .
#docker push registry.gitlab.com/shadowy/repository/$1:$2
docker buildx create --use
docker buildx build --platform linux/amd64,linux/arm,linux/arm64 --build-arg version=$2 -t registry.gitlab.com/shadowy/repository/$1:$2 -f ./docker/$1.dockerfile --push .

