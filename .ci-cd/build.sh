#!/bin/sh
CHART=$1
T="$(more $CHART/Chart.yaml | grep "^version:" || true)"
NEW_CHART_VERSION="$(echo "${T}" | awk '{print $2}')"
echo "New version: $NEW_CHART_VERSION"
helm repo add gitlab ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable --username gitlab-ci-token --password $CI_JOB_TOKEN
helm dependency update ${CHART}
T="$(helm show chart gitlab/$CHART | grep "^version:" || true)"
helm repo remove gitlab
OLD_CHART_VERSION="$(echo "${T}" | awk '{print $2}')"
echo "Current version: $OLD_CHART_VERSION"
if [ "${NEW_CHART_VERSION}" == "${OLD_CHART_VERSION}" ]; then
  echo "No updates"
  exit 0;
fi
FILE="$CHART-$NEW_CHART_VERSION.tgz"
echo "Build package $CHART version $NEW_CHART_VERSION ($FILE)"
helm package $CHART
curl --request POST --user gitlab-ci-token:$CI_JOB_TOKEN --form "chart=@${FILE}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
